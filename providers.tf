terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
    spacelift = {
      source  = "spacelift-io/spacelift"
      version = ">= 1.1.5"
    }
  }
}

provider "aws" {
  region = "us-west-2"
}
