variable "host_os" {
	type = string
	default = "linux"
}

variable "tags" {
  type = map(string)
  default = {
    tag1 = "something"
  }
}

locals {
  tags = {
    tag1 = "something"
    tag2 = "another_thing"
  }

  merged = merge(local.tags, var.tags)
}
